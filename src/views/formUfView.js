import View from "./View";

class FormUfView extends View {
  parentElement = document.querySelector(".uf-input-options");

  addLoadHandlerRender(handler) {
    window.addEventListener("load", handler);
  }

  addHandlerUFChange(handler) {
    this.parentElement.addEventListener("change", (e) => {
      const uf = e.target.value;
      handler(uf);
    });
  }

  generateMarkup() {
    return (
      `<option value="" selected disabled hidden>Selecione</option>
    ` +
      this.data
        .map(
          (uf) => `
        <option value="${uf.sigla}">${uf.sigla}</option>
    `
        )
        .join("")
    );
  }
}

export default new FormUfView();
