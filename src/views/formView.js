import { format } from "date-fns";
import IMask from "imask";

class FormView {
  parentElement = document.querySelector("#user-register-form");
  nameInputEl = document.querySelector("#name");
  cpfInputEl = document.querySelector("#cpf");
  birthInputEl = document.querySelector("#birth");
  ufInputEl = document.querySelector("#UF");
  cityInputEl = document.querySelector("#city");

  applyMaskCPF() {
    const maskOptions = {
      mask: "000.000.000-00",
    };
    const mask = IMask(this.cpfInputEl, maskOptions);
  }

  addHandlerSubmit(handler) {
    this.parentElement.addEventListener("submit", function (e) {
      e.preventDefault();
      const dataArr = [...new FormData(this)];
      const data = Object.fromEntries(dataArr);

      handler(data);
    });
  }

  clearFields() {
    this.nameInputEl.value = "";
    this.cpfInputEl.value = "";
    this.birthInputEl.value = "";
    this.ufInputEl.value = "";
    this.cityInputEl.value = "";
  }

  fillFromEditUserData(data) {
    const { name, cpf, birth, uf, city } = data;

    this.nameInputEl.value = name;
    this.cpfInputEl.value = cpf;
    this.birthInputEl.value = format(new Date(birth), "yyyy-MM-dd");
    this.ufInputEl.value = uf;
    this.cityInputEl.value = city;
  }
}

export default new FormView();
