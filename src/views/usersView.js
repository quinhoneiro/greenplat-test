import View from "./View.js";
import iconsDelete from "../assets/icon-1.svg";
import iconsEdit from "../assets/icon-2.svg";
import { format } from "date-fns";
import ptBR from "date-fns/locale/pt-BR/index.js";

class UsersView extends View {
  parentElement = document.querySelector(".users-data");
  editBtnElement;
  deleteBtnElement;
  errorMessage = "Nenhum usuário cadastrado!";

  generateMarkup() {
    return (
      `
    <tr>
      <th>Nome Completo</th>
      <th>CPF</th>
      <th>Data de Nascimento</th>
      <th>Idade</th>
      <th>Estado</th>
      <th>Cidade</th>
      <th>Editar</th>
      <th>Excluir</th>
    </tr>
    ` +
      this.data
        .map(
          (user, i) => `
            <tr data-index="${i}" class="user-row">
              <td>${user.name}</td>
              <td>${user.cpf}</td>
              <td>${format(new Date(user.birth), "P", { locale: ptBR })}</td>
              <td>${user.age}</td>
              <td>${user.uf}</td>
              <td>${user.city}</td>
              <td>
                <div class="icon">
                  <img id="edit" src="${iconsEdit}" alt="Imagem Editar" />
                </div>
              </td>
              <td>
                <div class="icon">
                  <img id="delete" src="${iconsDelete}" alt="Imagem Excluir" />
                </div>
              </td>
            </tr>
    `
        )
        .join("")
    );
  }

  addLoadHandlerRender(handler) {
    window.addEventListener("load", handler);
  }

  addHandlerDelete(handler) {
    this.deleteBtnElement = document.querySelectorAll("#delete");
    this.deleteBtnElement.forEach((btn) =>
      btn.addEventListener("click", (e) => {
        const index = e.target.closest(".user-row").getAttribute("data-index");
        handler(index);
      })
    );
  }

  addHandlerEdit(handler) {
    this.editBtnElement = document.querySelectorAll("#edit");
    this.editBtnElement.forEach((btn) =>
      btn.addEventListener("click", (e) => {
        const index = e.target.closest(".user-row").getAttribute("data-index");
        handler(index);
      })
    );
  }
}

export default new UsersView();
