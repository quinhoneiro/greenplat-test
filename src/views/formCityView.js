import View from "./View";

class FormCityView extends View {
  parentElement = document.querySelector(".city-input-options");

  generateMarkup() {
    return (
      `<option value="" selected disabled hidden>Selecione</option>
    ` +
      this.data
        .map(
          (city) => `
        <option value="${city.nome}">${city.nome}</option>
    `
        )
        .join("")
    );
  }
}

export default new FormCityView();
