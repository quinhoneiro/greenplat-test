import api from "./utils/api.js";

export const state = {
  users: [],
  isEditing: false,
  userEditing: "",
};

export const loadUF = async function () {
  try {
    const uf = await api.get("/estados?orderBy=nome");
    return uf.data;
  } catch (error) {
    throw new AppError("Ocorreu um erro ao carregar os estados.");
  }
};

export const loadCities = async function (uf) {
  try {
    const cities = await api.get(`/estados/${uf}/municipios?orderBy=nome`);
    return cities.data;
  } catch (error) {
    throw new AppError("Ocorreu um erro ao carregar as cidades.");
  }
};

export const addUser = function (user) {
  state.users.push(user);
  setDataToLocalStorage(state.users);
};

export const removeUser = function (index) {
  state.users.splice(index, 1);
  setDataToLocalStorage(state.users);
};

export const updateUser = function (user) {
  state.users[state.userEditing] = user;
  state.isEditing = false;
  state.userEditing = "";
  setDataToLocalStorage(state.users);
};

export const getUser = function (index) {
  const user = state.users[index];
  if (!user) throw new AppError("Usuário não encontrado.");

  state.isEditing = true;
  state.userEditing = index;

  return user;
};

export const setDataToLocalStorage = (user) => {
  localStorage.setItem("user", JSON.stringify(user));
};

export const getDataFromLocalStorage = () => {
  const user = localStorage.getItem("user");
  state.users = user ? JSON.parse(user) : [];
};
