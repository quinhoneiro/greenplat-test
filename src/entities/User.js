import { isBefore, addDays } from "date-fns";
import AppError from "../utils/appError";

class User {
  constructor(name, cpf, birth, uf, city) {
    this.name = name;
    this.cpf = this.checkCpf(cpf);
    this.birth = this.checkDate(birth);
    this.uf = uf;
    this.city = city;
    this.age = this.getAge(birth);
  }

  checkDate(birthDate) {
    if (!isBefore(addDays(new Date(birthDate), 1), new Date()))
      throw new AppError("Data Inválida");

    return addDays(new Date(birthDate), 1);
  }

  getAge(birthDate) {
    const today = new Date();
    const birth = new Date(birthDate);

    let age = today.getFullYear() - birth.getFullYear();
    const m = today.getMonth() - birth.getMonth();
    if (m < 0 || (m === 0 && today.getDate() < birth.getDate())) {
      age--;
    }
    return age;
  }

  checkCpf(cpf) {
    let sum = 0;
    let reminder;

    let strCPF = String(cpf).replace(/[^\d]/g, "");

    if (strCPF.length !== 11) throw new AppError("CPF Inválido!");

    if (
      [
        "00000000000",
        "11111111111",
        "22222222222",
        "33333333333",
        "44444444444",
        "55555555555",
        "66666666666",
        "77777777777",
        "88888888888",
        "99999999999",
      ].indexOf(strCPF) !== -1
    )
      throw new AppError("CPF Inválido!");

    let i;
    for (i = 1; i <= 9; i++)
      sum = sum + parseInt(strCPF.substring(i - 1, i)) * (11 - i);

    reminder = (sum * 10) % 11;

    if (reminder == 10 || reminder == 11) reminder = 0;

    if (reminder != parseInt(strCPF.substring(9, 10)))
      throw new AppError("CPF Inválido!");

    sum = 0;

    for (i = 1; i <= 10; i++)
      sum = sum + parseInt(strCPF.substring(i - 1, i)) * (12 - i);
    reminder = (sum * 10) % 11;

    if (reminder == 10 || reminder == 11) reminder = 0;

    if (reminder != parseInt(strCPF.substring(10, 11)))
      throw new AppError("CPF Inválido!");

    return cpf;
  }
}

export default User;
