import Toastify from "toastify-js";

export default class AppError {
  constructor(message) {
    this.message = message;
    this.toast();
  }

  toast() {
    Toastify({
      text: this.message,
      duration: 2000,
      close: true,
      gravity: "top",
      position: "right",
      stopOnFocus: true,
      style: {
        background: "linear-gradient(to right, #D32E2E, #D32E2E)",
      },
    }).showToast();
  }
}
