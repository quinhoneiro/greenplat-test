import User from "./entities/User.js";
import * as model from "./model.js";
import formView from "./views/formView.js";
import formCityView from "./views/formCityView.js";
import formUfView from "./views/formUfView.js";
import usersView from "./views/usersView.js";

const controlUsers = function () {
  usersView.render(model.state.users);
  usersView.addHandlerDelete(controlDeleteUser);
  usersView.addHandlerEdit(controlEditUser);
};

const controlForm = async function () {
  const uf = await model.loadUF();
  formUfView.render(uf);
  formCityView.render([{ nome: "" }]);
};

const controlUFChange = async function (uf) {
  const cities = await model.loadCities(uf);
  formCityView.render(cities);
};

const controlSubmit = function (data) {
  const { name, cpf, birth, UF, city } = data;

  if (!model.state.isEditing) {
    const user = new User(name, cpf, birth, UF, city);
    model.addUser(user);
    formView.clearFields();
    usersView.render(model.state.users);
    usersView.addHandlerDelete(controlDeleteUser);
    usersView.addHandlerEdit(controlEditUser);
    formCityView.render([{ nome: "" }]);
  } else {
    const user = new User(name, cpf, birth, UF, city);
    model.updateUser(user);
    formView.clearFields();
    usersView.render(model.state.users);
    usersView.addHandlerDelete(controlDeleteUser);
    usersView.addHandlerEdit(controlEditUser);
    formCityView.render([{ nome: "" }]);
  }
};

const controlEditUser = async function (index) {
  const user = model.getUser(index);
  const cities = await model.loadCities(user.uf);
  formCityView.render(cities);
  formView.fillFromEditUserData(user);
};

const controlDeleteUser = function (index) {
  model.removeUser(index);
  formView.clearFields();
  usersView.render(model.state.users);
  usersView.addHandlerDelete(controlDeleteUser);
  usersView.addHandlerEdit(controlEditUser);
};

const init = function () {
  model.getDataFromLocalStorage();
  usersView.addLoadHandlerRender(controlUsers);
  formUfView.addLoadHandlerRender(controlForm);
  formUfView.addHandlerUFChange(controlUFChange);
  formView.addHandlerSubmit(controlSubmit);
  formView.applyMaskCPF();
};
init();
